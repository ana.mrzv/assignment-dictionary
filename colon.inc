%define pointer 0
%macro colon 2
    %ifstr %1
        %2: dq pointer
        db %1, 0
    %else
        %error "First argument of colon is not a string"
    %endif
%define pointer %2
%endmacro 

;пример

;colon "third word", third_word
;db "third word explanation", 0

;third_word:
;dq 0
;db "third word", 0
;db "third word explanation", 0
