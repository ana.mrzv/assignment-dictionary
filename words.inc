section .data
colon "apple", apple
db "the round fruit of a tree of the rose family, which typically has thin green or red skin and crisp flesh", 0

colon "banana", banana
db "a long curved fruit which grows in clusters and has soft pulpy flesh and yellow skin when ripe", 0

colon "cucumber", cucumber
db "a long, green-skinned fruit with watery flesh, usually eaten raw in salads or pickled", 0
