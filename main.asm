%include "colon.inc"
%include "words.inc"

global _start
%include "lib.inc"
extern find_word

%define buffer_size 256

section .rodata
greeting: db "Введите ключ для поиска в словаре: ", 10, 0
found: db "Значение для ключа: ", 10, 0
not_found: db "Ключ отсутствует в словаре", 10, 0
error: db "Слишком длинный ключ", 10, 0

section .text
_start:
    mov rdi, greeting 
    call print_string ; записали и вывели приветствие

    sub rsp, buffer_size ; выделяем место для буфера
    mov rsi, buffer_size 
    mov rdi, rsp ; указатель на буфер
    call read_word ; считываем введенный ключ, указатель в rax
    test rax, rax
    jz .show_error_msg ; если слово больше буфера то ошибка

    mov rdi, rax ; передали начало слова 
    mov rsi, pointer ; передали указатель на нулевой/след. элемент из макроса
    call find_word ; ищем совпадение
    test rax, rax
    jz .show_not_found_msg ; выводим что не нашли

    add rsp, buffer_size ; возвращаем на место стек
    add rax, 8 ; если нашли то пропускаем ключ и указываем на значение
    push rax ; запоминаем адрес входа а то сбросится print_string'ом
    mov rdi, found
    call print_string
    
    pop rdi ; достали адрес
    push rdi 
    call string_length ; записали в rax длину ключа
    pop rdi
    inc rax ; учитываем 0
    add rdi, rax

    call print_string ; вывели само значение
    call print_newline
    call exit
    .show_error_msg:
        add rsp, buffer_size
        mov rdi, error
        call print_error
        call exit
    .show_not_found_msg:
        add rsp, buffer_size
        mov rdi, not_found
        call print_error
        call exit








