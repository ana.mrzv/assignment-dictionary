section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_int
global parse_uint
global string_copy
global print_error
 
exit: 
    mov rax, 60
    syscall
    ret

string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0 
        je .end
        inc rax
        jmp .loop
    .end:
        ret

print_string:
    call string_length 
    mov rsi, rdi 
    mov rdx, rax 
    mov rax, 1 
    mov rdi, 1 
    syscall
    ret

print_error:
    call string_length 
    mov rsi, rdi 
    mov rdx, rax 
    mov rax, 1 
    mov rdi, 2 ; stderr 
    syscall
    ret

print_char:
    mov rax, 1
    mov rdx, 1
    push rdi
    mov rdi, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret

print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
print_uint: 
    push r12
    push r13
    xor r13, r13 
    mov rax, rdi 
    mov r12, 10 
    .loop:
        xor rdx, rdx 
        div r12 
        add rdx, 48 
        push rdx 
        inc r13
        cmp rax, 0 
        jne .loop
    .output:
        pop rdi 
        call print_char
        dec r13
        cmp r13, 0
        jne .output
    pop r13
    pop r12
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0 
    js .neg 
    call print_uint
    ret
    .neg:
        push rdi
        mov rdi, 45 
        call print_char 
        pop rdi 
        neg rdi
        call print_uint
        ret

;  Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r12
    push r13
    xor rax, rax 
    xor rcx, rcx
    .loop:
        mov r13b, byte[rdi+rcx] 
        mov r12b, byte[rsi+rcx] 
        inc rcx 
        cmp r13, r12 
        je .check_null 
        pop r13
        pop r12
        ret 
    .check_null:
        test r13, r12 
        jnz .loop
        inc rax
        pop r13
        pop r12
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax 
    push rax 
    xor rdi, rdi 
    mov rdx, 1 
    mov rsi, rsp 
    syscall
    pop rax 
    ret 

; Принимает: адрес начала буфера rdi, размер буфера rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале 0x20, 0x9 и 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi    
    mov r13, rsi    
    xor r14, r14    
    .check_spaces:  
        call read_char
        cmp rax, 0x20
        je .check_spaces
        cmp rax, 0x9
        je .check_spaces
        cmp rax, 0xA
        je .check_spaces
        cmp rax, 0
        je .write_null
    .loop:
        cmp r14, r13 
        je .error
        mov [r12 + r14], rax 
        inc r14
        call read_char 
        cmp rax, 0
        je .write_null
        cmp rax, 0x20
        je .write_null
        cmp rax, 0x9
        je .write_null
        cmp rax, 0xA
        je .write_null
        jmp .loop
    .write_null:
        mov byte [r12 + r14], 0 
        mov rax, r12
        mov rdx, r14
        pop r14
        pop r13
        pop r12
        ret
    .error:
        mov rax, 0
        pop r14
        pop r13
        pop r12
        ret

; DONE Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx 
    xor rax, rax
    xor r8, r8 
    mov rsi, 0xA 
    .first:     
        mov r8b, [rdi]
        cmp r8b, '0'
        jb .no
        cmp r8b, '9'
        ja .no
        sub r8b, '0'
        mov al, r8b
        inc rcx
        jmp .next
    .next:      
        mov r8b, [rdi+rcx]
        cmp r8b, '0'
        jb .ok
        cmp r8b, '9'
        ja .ok
        inc rcx
        mul rsi 
        sub r8b, '0'
        add rax, r8
        jmp .next
    .no:
        mov rdx, 0
        ret
    .ok:
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается прочитать из её начала знаковое число.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rcx, rcx ; длина
    .sign:
        mov r8b, [rdi]
        inc rdi
        cmp r8b, '-'
        je .neg
        cmp r8b, '+'
        je .pos
        dec rdi 
    .pos:
        call parse_uint
        ret
    .neg:
        call parse_uint
        neg rax
        inc rdx
        ret
    
        
; Принимает указатель на строку, указатель на буфер и длину буфера 
;   rdi - ук. на строку
;   rsi - ук. на буфер
;   rdx - длина буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor r9, r9 
    call string_length 
    mov r9, rax
    inc rax 
    cmp rax, rdx 
    ja .too_long
    mov rcx, rax 
    xor r8, r8 
    .loop:
        test rcx, rcx
        jz .end
        mov r8b, [rdi] 
        inc rdi
        mov byte [rsi], r8b 
        inc rsi
        dec rcx
        jmp .loop
    .too_long:
        xor rax, rax
        ret 
    .end:
        mov rax, r9
        ret
